/* fix for floating navbar */
var offset = 53;
$('.navbar li a').click(function(event) {
  event.preventDefault();
  var $anchor = $(this);
  $('html, body').stop().animate({
    scrollTop: $($anchor.attr('href')).offset().top - offset
  }, 1500, 'easeOutQuint');
  event.preventDefault();
});

$('.navbar li a.dropdown-toggle').on('addClass toggleClass removeClass', function (e) {
  if($(e.target).hasClass('active')) {
    $(e.target).parent().addClass('open');
    $('.dropdown-container').addClass('open');
  } else {
    $(e.target).parent().removeClass('open');
    $('.dropdown-container').removeClass('open');
  }
});

/* fix for outside indicators */
$('.carousel').on('slide.bs.carousel', function (event) {
  var id = $(event.target).attr('id');
  $('.carousel-indicators [data-target="#'+id+'"]').removeClass('active');
  var target = $(event.relatedTarget).index();
  $('.carousel-indicators [data-target="#'+id+'"][data-slide-to="'+target+'"]').addClass('active');
});
/* lorem runner */
(function($) {
  $(document.body).ipsum();
}(jQuery));
